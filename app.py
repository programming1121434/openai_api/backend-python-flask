import os
from dotenv import load_dotenv
from dotenv import dotenv_values
import openai
from flask import Flask, jsonify, request
from flask_cors import CORS
from prometheus_flask_exporter import PrometheusMetrics

# load_dotenv()
# openai.api_key = os.getenv("OPENAI_API_KEY")

env_vars = dotenv_values("/app/config/.env")
openai.api_key = env_vars.get("OPENAI_API_KEY")


app = Flask(__name__)
CORS(app)
metrics = PrometheusMetrics(app)
metrics.info("app_info", "App Info, this can be anything you want", version="1.0.0")

@app.route("/result/", methods=['POST'])
def generate_story():
    data = request.get_json()
    name = data.get('nameOfDeath', '')
    occupation = data.get('occupation', '')
    cause_of_death = data.get('causeOfDeath', '')
    place_of_death = data.get('placeOfDeath', '')
    datetodie = data.get('datetodie', '')

    prompt = f'ตั้งชื่อเรื่อง (เว้นบรรทัด) และเล่าเรื่องผีให้น่ากลัวจากการใช้คำเหล่านี้: ผู้ตายชื่อ{name}เป็น{occupation}ตายเพราะ{cause_of_death}เสียชีวิตที่{place_of_death}ในวันที่{datetodie}'

    response = openai.ChatCompletion.create(
        model = "gpt-3.5-turbo",
        messages = [
            {
                "role" : "system",
                "content" : "You are a helpful assistant"
            },
            {
                "role" : "user",
                "content" : prompt,
            }
        ]
    )

    result = response['choices'][0]['message']['content']
    return jsonify({"result": result})

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80, debug=True)